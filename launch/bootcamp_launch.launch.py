# Copyright 2022 Martin Endler
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Launch file for vehicle for Autoware Bootcamp."""

import os
from typing import List

from ament_index_python import get_package_share_directory
from launch import LaunchDescription, SomeSubstitutionsType
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import PathJoinSubstitution
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare


def get_shared_file(package_name: str, *path: List[str]) -> str:
    """Return the path to the given shared file."""
    return os.path.join(
        get_package_share_directory(package_name),
        *path,
    )


def get_shared_file_substitution(package_name: str, *path: List[str]) -> SomeSubstitutionsType:
    """Return a Substitution that resolves to the path to the given shared file."""
    return PathJoinSubstitution([
        FindPackageShare(package=package_name),
        *path,
    ])


def generate_launch_description():

    dataspeed_dbw_ford = Node(
        executable='dbw_node',
        name='dataspeed_dbw_ford_node',
        namespace='dbw',
        package='dbw_ford_can',
        output='screen',
        parameters=[
            get_shared_file_substitution(
                'bootcamp_launch', 'config', 'dbw_config.yaml',
            ),
        ],
        remappings=[
            # ('points_in', 'points_raw')
        ],
    )

    # TODO: refactor (declarative way, avoid side effects, use dynamic substition)
    urdf_path = get_shared_file('bootcamp_launch', 'urdf', 'lincoln_mkz.urdf')
    with open(urdf_path, 'r') as urdf_file_handle:
        urdf_file_content = urdf_file_handle.read()

    urdf_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name='robot_state_publisher',
        parameters=[
            {
                # Parameter robot_description as file path, not URDF string
                # https://github.com/ros/robot_state_publisher/issues/155
                'robot_description': urdf_file_content,
            },
        ],
    )

    ouster_launch = IncludeLaunchDescription(
        launch_description_source=PythonLaunchDescriptionSource(
            launch_file_path=get_shared_file_substitution(
                'ros2_ouster', 'launch', 'driver_launch.py'
            ),
        ),
        launch_arguments=[
            (
                'params_file',
                get_shared_file_substitution(
                    'bootcamp_launch', 'config', 'ouster_config.yaml',
                ),
            ),
        ],
    )

    filter_transform_ouster = Node(
        package='point_cloud_filter_transform_nodes',
        executable='point_cloud_filter_transform_node_exe',
        name='filter_transform_ouster',
        namespace='lidar',
        parameters=[
            get_shared_file_substitution(
                'bootcamp_launch',
                'config',
                'filter_transform_params.yaml'
            ),
        ],
        remappings=[('points_in', '/points')],
    )

    return LaunchDescription([
        dataspeed_dbw_ford,
        urdf_publisher,
        ouster_launch,
        filter_transform_ouster,
    ])
